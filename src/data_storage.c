#include "main.h"
#include "data_storage.h"
#include "display.h"

#include <avr/wdt.h>

#if 0
#include <avr/eeprom.h>
#else
#include "drivers/EEPROM_25AA640A.h"
#endif

/**
 * Contains the functions used to store patterns in the EEPROM indefinitely
 */

static uint8_t activeBank = 0; /* Bank being displayed currently */
static uint8_t toStoreBank = 0; /* Bank where incoming data will be stored */

/**
 * Calculates the address of the given screen in EEPROM
 * 
 * @param bank Bank in EEPROM to calculate address in
 * @param screen Screen number whose address is to be calculated
 * 
 * @return Address for the request bank / screen combination
 */
uint16_t calcScreenAddress(uint8_t bank,
                           uint16_t screen) {
    
    uint16_t outAddress;
    
    if (bank == 0) {
        outAddress = EE_ADDR_ARRAY1;
    } else {
        outAddress = EE_ADDR_ARRAY2;
            
        /* If invalid toStore location is given it defaults to bank 1.
            * Even if a valid 1 is given, toStoreBank will get 
            * overwritten since it would take more instructions to check 
            * if it wasn't valid */
        toStoreBank = 1;
    }
        
    outAddress += EE_OFFSET_BANK_DATA;
    outAddress += screen * (EE_PATERN_SIZE + 1);
    
    return outAddress;            
}

/**
 * Stores the next screen (pattern and delay) in the next spot in EEPROM
 * 
 * Until numberOfScreens screens have been stored, each call of this
 * function will store the given screen after the previously stored 
 * pattern. To abort a storage, set numberOfScreens to 0.
 * 
 * @param numberOfScreens: Size (in screens) of current display. 
 *                          Set to 0 to abort storage of a pattern
 * @param inPattern: Pattern for current screen
 * @param inDelay: Delay for current screen
 */
void storeNextScreenInEEPROM(uint16_t numberOfScreens, 
                             uint8_t inPattern[EE_PATERN_SIZE],
                             uint8_t inDelay) {
    
    static uint16_t screenToStore = 0;
    
    /* Don't store more screens than there is room for */
    if ((numberOfScreens > 0) && (screenToStore < EE_BANK_SCREENS)) {
        /* @TODO: See if writing pattern and delay at same time is better 
         * than pattern then delay. Memory usage has higher priority 
         * over speed */
        
        /* Determine where the next screen goes */
        uint16_t destAddress = calcScreenAddress(toStoreBank, 
                                                 screenToStore);
                                                 
        /* Store the number of screens that are being stored when storing
         * the first screen */
        if (screenToStore == 0) {
            eeprom_25aa640a_write_byte((void *) 
                                    (destAddress - EE_OFFSET_BANK_DATA),
                                    numberOfScreens);
        }
        
        /* Write the pattern */
        eeprom_25aa640a_write_block(inPattern, 
                            (void *) destAddress, 
                            EE_PATERN_SIZE);
                            
        /* Then the delay */
        eeprom_25aa640a_write_byte((void *) (destAddress + EE_PATERN_SIZE),
                            inDelay);
                            
        /* Next time this function is run, store the next screen */
        screenToStore++;
        
        /* After all screens have been stored, reset the screen counter */
        if (screenToStore >= EE_BANK_SCREENS) {
            screenToStore = 0;
        }
    }
}

/**
 * Reads the next screen (pattern and delay) from the EEPROM
 * 
 * @param outPattern Address to store just read pattern from EEPROM
 * @param outDelay Address to store just read delay from EEPROM
 * @returns Number of patterns in current display
 */
uint16_t readNextScreenFromEEPROM(uint8_t *outPattern,
                                  uint8_t *outDelay) {

    static uint16_t screenToRead = 0; /* Screen number to store */
    static uint16_t numberOfScreens = 0; /* Number of screens in display */
    
    uint16_t sourceAddress = calcScreenAddress(activeBank,
                                               screenToRead);
    
    /* If no screens have been read for display, get the number of
     * screens in display which is the first byte for the bank */
    if (screenToRead == 0) {
        numberOfScreens = eeprom_25aa640a_read_byte((void *) 
                                (sourceAddress - EE_OFFSET_BANK_DATA));
    }
    
    /* Load the current screen */
    if (screenToRead < numberOfScreens) {                                                           
                                                   
        /* Load the pattern */
        eeprom_25aa640a_read_block(outPattern,
                          (void *) sourceAddress,
                          EE_PATERN_SIZE);
                          
        /* Load the delay */
        *outDelay = eeprom_25aa640a_read_byte((void *) 
                                     (sourceAddress + EE_PATERN_SIZE));
                                     
        screenToRead++;
        
        if (screenToRead == numberOfScreens) {
            screenToRead = 0;
        }
    }
    
    return numberOfScreens;
}

/**
 * Returns whether if the EEPROM is populated
 */
bool eepromIsPopulated(void)
{
    bool retVal;
    uint8 bytesStored;
    uint16_t screenCountAddress;

    retVal = false;
    
    screenCountAddress = calcScreenAddress(0, 0) 
                                            - EE_OFFSET_BANK_DATA;

    bytesStored = eeprom_25aa640a_read_byte((void *) screenCountAddress);

    /* Load the number of screens */
    if((bytesStored > 0) && (bytesStored < 255)) {
        retVal = true;
    }

    return retVal;
}

/**
 * Removes the stored screens from EEPROM and returns the display to the
 * default display
 *
 * Resets the board
 *
 */
void clearScreen()
{
    /* Set the number of screens in EEPROM to zero */
    eeprom_25aa640a_write_byte((uint16_t*) EE_ADDR_ARRAY1, 0);

    /* Activate WDT and allow it to reset the chip. Set it to the shortest
    possible time */
    wdt_reset();
    wdt_enable(WDTO_250MS);

    #if 0
    for(;;) {
        /* Wait for the end to come... */
    }
    #endif
    

}

/**
 * Disables the WDT timer
 */
void initEEPROM()
{
    MCUSR &= ~_BV(WDRF);
    wdt_disable(); /* Disable the watchdog timer */

    /* If data is in the EEPROM load it */
}
