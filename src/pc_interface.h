#ifndef PC_INTERFACE_H
#define PC_INTERFACE_H

#include <stdbool.h>
#include <stdint.h>

#define SERIAL_READY_CHAR 'S'

//#define WAITING_CHAR "W"
#define COMPLETE_CHAR 'T'
#define XOR_CHAR 'X'

#define SERIAL_CLEAR_EEPROM 'C'
#define SERIAL_LOAD_EEPROM 'L'
#define SERIAL_STORE_EEPROM 'R'


#define UART_RX_BUFFER_SIZE 100
#define TIME_OUT_MS 1000

volatile bool dataInRXBuffer;

typedef struct {
    uint8_t front;
    uint8_t count;
    char data[UART_RX_BUFFER_SIZE];
} circular_buffer;

volatile circular_buffer serial_rx_buffer;

volatile uint8_t serialScreenCount;

void serial_buffer_parser(void);
void downloadNewBoard(void);

void buffer_init(void);
uint8_t buffer_add_to(char toAdd);
char buffer_get_next(void);
bool buffer_is_empty(void);
bool buffer_is_full(void);

void print_a_screen(uint8_t pattern[13], uint8_t delay);

#endif /* PC_INTERFACE_H */
