/*
 * SPI.h
 *
 * Created: 8/17/2015 3:18:38 PM
 *  Author: Meyer
 */

#include <stdbool.h>
#include <stdint.h>
#include <avr/io.h>

#ifndef SPI_H_
#define SPI_H_

/**
 * @def SPI_PORT
 * Port for the SPI
 */
#define SPI_PORT PORTB

/**
 * @def SPI_DDR
 * DDR for the SPI
 */
#define SPI_DDR DDRB

/**
 * @def SPI_SCK
 * Pin attached to the SPI clock
 */
#define SPI_SCK PORTB1

/**
 * @def SPI_MOSI
 * Pin attached to the SPI MOSI
 */
#define SPI_MOSI PORTB2

/**
 * @def SPI_MISO
 * Pin attached to the SPI MISO
 */
#define SPI_MISO PORTB3

/**
 * @def SPI_SS
 * Pin attached to the SPI SS
 *
 * Unused but needs to be made an output
 */
#define SPI_SS PORTB0

void spi_init(void);
void spi_transmit(uint8_t outByte);
uint8_t spi_receive(void);

#endif /* SPI_H_ */
