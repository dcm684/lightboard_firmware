/*
 * EEPROM_25AA640A.c
 *
 * Created: 8/17/2015 12:34:55 PM
 *  Author: Meyer
 */
#include "../main.h"

#include <stdbool.h>
#include <stdint.h>
#include <util/delay.h>
#include <avr/pgmspace.h>

#include "EEPROM_25AA640A.h"
#include "SPI.h"

//#define DEBUG_EEPROM

void eeprom_25aa640a_set_active(void)
{
    EEPROM_PORT &= ~_BV(EEPROM_CS);
}

void eeprom_25aa640a_set_inactive(void)
{
    EEPROM_PORT |= _BV(EEPROM_CS);
}


void eeprom_25aa640a_init(void)
{
    /* Set the EEPROM's CS, HOLD, and WP pins to outputs */
    EEPROM_DDR |= _BV(EEPROM_CS);
    EEPROM_DDR |= _BV(EEPROM_HOLD);
    EEPROM_DDR |= _BV(EEPROM_WP);

    /* Make EEPROM inactive*/
    eeprom_25aa640a_set_inactive();
    EEPROM_PORT |= _BV(EEPROM_HOLD); /* Don't hold */
    EEPROM_PORT |= _BV(EEPROM_WP); /* Allow writes */

    eeprom_25aa640a_set_active();
    eeprom_25aa640a_set_inactive();
}

/**
 * Enables / disables writing to the EEPROM's memory and status registers
 */
void eeprom_25aa640a_set_wren(bool enable)
{
    eeprom_25aa640a_set_active();

    if(enable) {
        spi_transmit(EEPROM_INST_ENABLE_WRITE);
        //EEPROM_PORT |= _BV(EEPROM_WP); /* Allow writes */
    } else {
        spi_transmit(EEPROM_INST_DISABLE_WRITE);
        //EEPROM_PORT &= ~_BV(EEPROM_WP);
    }

    eeprom_25aa640a_set_inactive();
}

/**
 * Returns the current value of the EEPROM's STATUS register
 */
uint8_t eeprom_25aa640a_read_status(void)
{
    uint8_t outVal;

    eeprom_25aa640a_set_active();

    spi_transmit(EEPROM_INST_STATUS_READ);
    spi_transmit(0);
    outVal = spi_receive();

    eeprom_25aa640a_set_inactive();

#ifdef DEBUG_EEPROM
    printf_P(PSTR("Read Status: 0x%02X\r\n"), outVal);
#endif /* DEBUG_EEPROM */

    return outVal;
}

/**
 * Holds the system while an EEPROM write is in progress
 *
 * Blocking hold that checks Bit 0 (WIP) in the EEPROM's status register.
 * When WIP is 0 the function is exited
 */
void eeprom_25aa640a_hold_while_wip(void)
{
    while(eeprom_25aa640a_read_status() & 0x01) {}
}

/**
 * Writes the given value to the EEPROM's STATUS register
 */
void eeprom_25aa640a_write_status(uint8_t status)
{
    eeprom_25aa640a_hold_while_wip();

    eeprom_25aa640a_set_active();

    spi_transmit(EEPROM_INST_STATUS_WRITE);
    spi_transmit(status);

    eeprom_25aa640a_set_inactive();

#ifdef DEBUG_EEPROM
    printf_P(PSTR("Wrote Status: 0x%02X\r\n"), status);
#endif /* DEBUG_EEPROM */
}

/**
 * Returns the data stored at the given address in EEPROM
 *
 * If the given address is out of range, 0x00 will be returned. The onus
 * is on the calling function to determine validity of the given address.
 *
 * @param __p Pointer to where the data should be read from
 *
 * @return Byte of data read from EEPROM. Will be 0x00 if __p is out of
 *			range
 */
uint8_t eeprom_25aa640a_read_byte(const uint16_t *__p)
{
    if(__p > (const uint16_t *) EEPROM_CAPACITY) {
        return 0x00;
    }

    uint8_t tempByte;

    eeprom_25aa640a_read_block(&tempByte, (void *) __p, 1);

#ifdef DEBUG_EEPROM
    printf_P(PSTR("Read Byte (%X): %u\r\n"), __p, tempByte);
#endif /* DEBUG_EEPROM */

    return tempByte;
}

/**
 * Reads a block of data from the EEPROM
 *
 * @param __dest Location to store the read data
 * @param __src Location in EEPROM where read is to start.
 * @param __n Number of bytes to read from EEPROM. If __n puts the final
 *				byte read out of range for the EEPROM, only the bytes
 *				available to the end of the EEPROM will be read
 *
 * @return Number of bytes read
 */
size_t eeprom_25aa640a_read_block(void *__dst, const void *__src, size_t __n)
{
    uint16_t eeprom_addr = (uint16_t) __src;

    if((eeprom_addr >= EEPROM_CAPACITY) || (__n == 0)) {
        return 0;
    } else if(EEPROM_CAPACITY - eeprom_addr <= __n) {
        /* Only read the number of bytes actually available */
        __n = EEPROM_CAPACITY - eeprom_addr;
    }

    uint8_t addressTemp;
    uint8_t *dst = __dst; /* uint8_t cast destination */

    eeprom_25aa640a_hold_while_wip();

    eeprom_25aa640a_set_active();

    /* Send the read EEPROM command */
    spi_transmit(EEPROM_INST_READ);

    /* Send the 16-bit address to start read at */
    addressTemp = eeprom_addr >> 8;
    spi_transmit(addressTemp);

    addressTemp = eeprom_addr & 0xFF;
    spi_transmit(addressTemp);

    size_t i;

    /* Read __n bytes of data from the EEPROM */
    for(i = 0; i < __n; i++) {
        /* Send dummy data to clock the data in */
        spi_transmit(0);
        *dst = spi_receive();
        dst++;
    }

    eeprom_25aa640a_set_inactive();

    return __n;
}

/**
 * Write the given value to the given address in EEPROM
 *
 * @param __p Address to write to
 * @param __value Value to write
 */
void eeprom_25aa640a_write_byte(uint16_t *__p, uint8_t __value)
{
    eeprom_25aa640a_write_block(&__value, (void *)__p, 1);

#ifdef DEBUG_EEPROM
    printf_P(PSTR("Wrote Byte (%X): %u\r\n"), __p, __value);
#endif /* DEBUG_EEPROM */

}

/**
 * Write the given set of data starting at the given address in EEPROM
 *
 * @param __src Pointer of where data is in RAM to write
 * @param __dst Address in EEPROM to write the data to
 * @param __n Number of bytes to write toEEPROM. If __n puts the final
 *				byte written out of range for the EEPROM, only the bytes
 *				available to the end of the EEPROM will be written
 *
 * @return Number of bytes written
 */
size_t eeprom_25aa640a_write_block(const void *__src, void *__dst, size_t __n)
{
    uint8_t addressTemp; /* Stores chunks of EEPROM address to be sent */
    uint16_t eepromAddr = (uint16_t) __dst; /* EEPROM address as uint16 */
    uint8_t placeInPage; /* Location in page being written to */
    size_t bytesWritten = 0; /* Number of bytes that have been */

    /* Validate EEPROM address and number of bytes to write */
    if((eepromAddr >= EEPROM_CAPACITY) || (__n <= 0)) {
        return 0;
    } else if(EEPROM_CAPACITY - eepromAddr <= __n) {
        /* Only write the number of bytes actually available */
        __n = EEPROM_CAPACITY - eepromAddr;
    }

    /* Determine current place in the first page */
    placeInPage = eepromAddr % EEPROM_BYTES_PER_PAGE;

    while(bytesWritten < __n) {
        /* Each page requires a whole new SPI command to start a write */
        eeprom_25aa640a_hold_while_wip();

        /* Enable writes */
        eeprom_25aa640a_set_wren(true);

        eeprom_25aa640a_set_active();

        spi_transmit(EEPROM_INST_WRITE);

        /* Send the 16-bit address to start write at */
        addressTemp = eepromAddr >> 8;
        spi_transmit(addressTemp);

        addressTemp = eepromAddr & 0xFF;
        spi_transmit(addressTemp);

        /* Only one page can be written at a time. The chip writes in a
         * looping fashion, e.g. 30, 31, 0, 1, etc.
         */
        while(placeInPage < EEPROM_BYTES_PER_PAGE) {
            /* Send byte to write */
            spi_transmit(*((uint8_t*) __src));
            __src = (uint8_t *)__src + 1;
            placeInPage++;
            bytesWritten++;
            eepromAddr++; /* @TODO: Updated more efficiently,
									one time per page */

            /* Stop writing if all of the data has been written */
            if(bytesWritten >= __n) {
                break;
            }
        }

        placeInPage = 0;

        eeprom_25aa640a_set_inactive();
    }

    return bytesWritten;
}