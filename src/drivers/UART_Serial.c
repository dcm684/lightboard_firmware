/*
 * UART_Serial.c
 *
 * All UART serial functions
 *
 * Created: 10/8/2015 3:42:32 PM
 *  Author: Meyer
 */

#include "UART_Serial.h"

#include "main.h"
#include "../pc_interface.h"

extern void disableDisplayInt(void);
extern void enableDisplayInt(void);

/**
 * Initialize the on microcontroller UART
 */
void uart_initialize0(void)
{

    UART_DDR_REG |= _BV(UART_DDR_TX);
    UART_DDR_REG |= ~_BV(UART_DDR_RX);

    UART_POWER_REG &= ~(1 << UART_POWER_BIT); /* Turn on UART */

    //Set baud rate
    UART_BAUD_REG_L = (unsigned char) UBRRVAL;		//low byte
    UART_BAUD_REG_H = (unsigned char)(UBRRVAL >> 8);	//high byte

    //Enable Transmitter and Receiver
    UART_RX_TX_EN_REG = _BV(UART_TX_EN_BIT) | _BV(UART_RX_EN_BIT);

    //Set data frame format: asynchronous mode,no parity, 1 stop bit,
    //8 bit size
    UART_FRAME_FORMAT_REG = UART_FRAME_FORMAT_VAL;



    /* Enable Receive Interrupts */
    UART_RX_ISR_REG |= _BV(UART_RX_ISR_BIT);

    uartOne = fdevopen(uart_putc0, uart_getc0);  /* printf for hardware UART */
}

/**
 * Send a Char through the UART
 *
 * @param inChar Char to send
 * @param unused UNUSED
 * @returns Always 0
 */
int uart_putc0(char inChar, FILE *unused)
{
    // Wait if a byte is being transmitted
    while((UART_TX_REG_EMPTY_REG & (1 << UART_TX_REG_EMPTY_BIT)) == 0);

    // Transmit data
    UART_IO_DATA_REG = inChar;

    return 0;
}

/**
 * Receive a char through the UART
 *
 * @param unused UNUSED
 * @returns Char received over the UART
 */
int uart_getc0(FILE *unused)
{
    // Wait until a byte has been received
    while((UART_RX_COMPLETE_REG & (1 << UART_RX_COMPLETE_BIT)) == 0) ;

    // Return received data
    return UART_IO_DATA_REG;
}

/**
 * ISR for serial receive events
 *
 * Paused the display update and puts the received char in the receive
 * char buffer
 */
ISR(UART_RX_ISR_VECTOR)
{
    disableDisplayInt(); //Stop updating the screen when data is received

    buffer_add_to(uart_getc0(NULL));

    enableDisplayInt(); //Resume screen update
}