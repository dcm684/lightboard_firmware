/*
 * SPI.c
 *
 * Created: 8/17/2015 3:18:47 PM
 *  Author: Meyer
 */
#include <stdbool.h>
#include <stdint.h>

#include "SPI.h"


#include "../display.h"

void spi_init(void)
{
    /* Power on the SPI module */
    PRR0 &= ~_BV(PRSPI);

    /* Set SCK and MOSI to outputs and MISO to an input */
    SPI_DDR |= _BV(SPI_MOSI);
    SPI_DDR |= _BV(SPI_SCK);
    SPI_DDR &= ~_BV(SPI_MISO);

    SPI_PORT &= ~_BV(SPI_MISO); /* Don't let the pull up be used on MISO pin */

    SPI_DDR |= _BV(SPI_SS); /* Make the SS pin an output. Its unused */
    SPI_PORT |= _BV(SPI_SS);

    /* Set up the SPI functionality on the chip */
    SPSR  = 0; /* Disable Double SPI Baud Rate */

    /*
    SPI Interrupts off
    SPI Enabled
    MSB first
    Master
    Leading edge is rising edge
    Sample on leading, setup on trailing
    fosc/4
    */
    //SPCR = 0b01010000;
    SPCR = 0; /* Set SCK frequency to fosc / 4 and clear out everything else */
    SPCR |=	(_BV(SPE)  /* SPI Enable */
             |	_BV(MSTR) /* SPI Master */
            );



    /* Clear the SPI Interrupt flag by reading the SPSR then the SPDR */
    uint8_t trash;
    trash = SPSR;
    trash = SPDR;
}

/**
 * Transmit the given byte over SPI
 *
 * Currently is a blocking send
 */
void spi_transmit(uint8_t outByte)
{
    SPDR = outByte;

    /* Wait for the transmit to complete before exiting */
    while(!(SPSR & (_BV(SPIF))));
}

/**
 * Returns the byte sent over SPI
 *
 * Currently is blocking
 */
uint8_t spi_receive(void)
{
    /* Wait for a byte to be received */
    while(!(SPSR & _BV(SPIF)));

    return SPDR;
}
