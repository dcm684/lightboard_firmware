/*
 * UART_Serial.h
 *
 * Created: 10/8/2015 3:42:23 PM
 *  Author: Meyer
 */

#include <stdio.h>

#ifndef UART_SERIAL_H_
#define UART_SERIAL_H_

#if 1 /* ATMEGA32U2 */

#define UART_DDR_REG DDRD
#define UART_DDR_RX DDD2
#define UART_DDR_TX DDD3


/**
 * @def UART_POWER_REG
 * Register containing bit used to enable/disable the UART
 */
#define UART_POWER_REG PRR1

/**
 * @def UART_POWER_BIT
 * Bit in UART_POWER_REG used to enable/disable the UART
 */
#define UART_POWER_BIT PRUSART1

/**
 * @def UART_BAUD_REG_H
 * Register that contains the high byte of the baud rate
 */
#define UART_BAUD_REG_H UBRR1H

/**
 * @def UART_BAUD_REG_L
 * Register that contains the low byte of the baud rate
 */
#define UART_BAUD_REG_L UBRR1L

/**
 * @def UART_RX_TX_EN_REG
 * Register that contains the bits to enable the UART's TX and RX
 */
#define UART_RX_TX_EN_REG UCSR1B

/**
 * @def UART_RX_EN_BIT
 * Bit in UART_RX_TX_EN_REG used to enable receiving data on the UART
 */
#define UART_RX_EN_BIT RXEN1

/**
 * @def UART_TX_EN_BIT
 * Bit in UART_RX_TX_EN_REG used to enable transmitting data on the UART
 */
#define UART_TX_EN_BIT TXEN1

/**
 * @def UART_FRAME_FORMAT_REG
 * Register that contains the byte used to format the UART messages
 */
#define UART_FRAME_FORMAT_REG UCSR1C

/**
 * @def UART_FRAME_FORMAT_VAL
 * Value to set the UART frame register to
 * UMSEL1X - Asynch
 * UPM1X - Parity disabled
 * USBS1 - 1 stop bit
 * UCSZ1X - 8-bit chars
 */
#define UART_FRAME_FORMAT_VAL ((0<<UMSEL11)|(0<<UMSEL10)\
									|(0<<UPM11)|(0<<UPM10)\
									|(0<<USBS1)\
									|(1<<UCSZ11)|(1<<UCSZ10)\
									|(0<<UCPOL1))

/**
 * @def UART_RX_ISR_REG
 * Register containing the bit used to enable the UART's interrupt on
 * received bytes
 */
#define UART_RX_ISR_REG UCSR1B

/**
 * @def UART_RX_ISR_BIT
 * Bit in UART_RX_ISR_REG used to enable the UART's interrupt on received
 * bytes
 */
#define UART_RX_ISR_BIT RXCIE1

/**
 * @def UART_TX_REG_EMPTY_REG
 * Register containing the bit indicating that the TX register is empty
 */
#define UART_TX_REG_EMPTY_REG UCSR1A

/**
 * @def UART_TX_REG_EMPTY_BIT
 * Bit in UART_TX_REG_EMPTY_REG indicating that the TX register is empty
 */
#define UART_TX_REG_EMPTY_BIT UDRE1

/**
 * @def UART_RX_COMPLETE_REG
 * Register containing the bit indicating that a receive has been completed
 */
#define UART_RX_COMPLETE_REG UCSR1A

/**
 * @def UART_RX_COMPLETE_BIT
 * Bit in UART_RX_COMPLETE_REG indicating that a byte has been received
 */
#define UART_RX_COMPLETE_BIT RXC1

/**
 * @def UART_IO_DATA_REG
 * Register used to store received bytes and bytes to be sent
 */
#define UART_IO_DATA_REG UDR1

/**
 * @def UART_RX_ISR_VECTOR
 * Vector for UART receive interrupts
 */
#define UART_RX_ISR_VECTOR USART1_RX_vect
#else

/**
 * @def UART_POWER_REG
 * Register containing bit used to enable/disable the UART
 */
#define UART_POWER_REG PRR

/**
 * @def UART_POWER_BIT
 * Bit in UART_POWER_REG used to enable/disable the UART
 */
#define UART_POWER_BIT PRUSART0

/**
 * @def UART_BAUD_REG_H
 * Register that contains the high byte of the baud rate
 */
#define UART_BAUD_REG_H UBRR0H

/**
 * @def UART_BAUD_REG_L
 * Register that contains the low byte of the baud rate
 */
#define UART_BAUD_REG_L UBRR0L

/**
 * @def UART_RX_TX_EN_REG
 * Register that contains the bits to enable the UART's TX and RX
 */
#define UART_RX_TX_EN_REG UCSR0B

/**
 * @def UART_RX_EN_BIT
 * Bit in UART_RX_TX_EN_REG used to enable receiving data on the UART
 */
#define UART_RX_EN_BIT RXCIE0

/**
 * @def UART_TX_EN_BIT
 * Bit in UART_RX_TX_EN_REG used to enable transmitting data on the UART
 */
#define UART_TX_EN_BIT TXCIE0

/**
 * @def UART_FRAME_FORMAT_REG
 * Register that contains the byte used to format the UART messages
 */
#define UART_FRAME_FORMAT_REG UCSR0C

/**
 * @def UART_FRAME_FORMAT_VAL
 * Value to set the UART frame register to
 */
#define UART_FRAME_FORMAT_VAL ((0<<UMSEL01)|(0<<UMSEL00)|(0<<UPM01)\
									|(0<<UPM00)|(0<<USBS0)|(0<<UCSZ02)\
									|(1<<UCSZ01)|(1<<UCSZ00))

/**
 * @def UART_RX_ISR_REG
 * Register containing the bit used to enable the UART's interrupt on
 * received bytes
 */
#define UART_RX_ISR_REG UCSR0B

/**
 * @def UART_RX_ISR_BIT
 * Bit in UART_RX_ISR_REG used to enable the UART's interrupt on received
 * bytes
 */
#define UART_RX_ISR_BIT RXCIE0

/**
 * @def UART_TX_REG_EMPTY_REG
 * Register containing the bit indicating that the TX register is empty
 */
#define UART_TX_REG_EMPTY_REG UCSR0A

/**
 * @def UART_TX_REG_EMPTY_BIT
 * Bit in UART_TX_REG_EMPTY_REG indicating that the TX register is empty
 */
#define UART_TX_REG_EMPTY_BIT UDRE0

/**
 * @def UART_RX_COMPLETE_REG
 * Register containing the bit indicating that a receive has been completed
 */
#define UART_RX_COMPLETE_REG UCSR0A

/**
 * @def UART_RX_COMPLETE_BIT
 * Bit in UART_RX_COMPLETE_REG indicating that a byte has been received
 */
#define UART_RX_COMPLETE_BIT RXC0

/**
 * @def UART_IO_DATA_REG
 * Register used to store received bytes and bytes to be sent
 */
#define UART_IO_DATA_REG UDR0

/**
 * @def UART_RX_ISR_VECTOR
 * Vector for UART receive interrupts
 */
#define UART_RX_ISR_VECTOR USART0_RX_vect

#endif

FILE* uartOne;

//set desired baud rate
#define BAUDRATE 500000
//calculate UBRR value
#define UBRRVAL ((F_CPU / (BAUDRATE * 16UL)) - 1)

#define SERIAL_ISR_VECTOR 'USART0_RXC_vect'
#define USART_RX_INT_EN 'RXCIE0'
#define USART_STATUS_B 'UCSR0B'
#define SERIAL_DATA_REG 'UDR0'

void uart_initialize0(void);
int uart_putc0(char, FILE*);
int uart_getc0(FILE*);

#endif /* UART_SERIAL_H_ */