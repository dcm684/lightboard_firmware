/*
 * USB.c
 *
 * Created: 10/5/2015 2:47:05 PM
 *  Author: Meyer
 */
#include <avr/pgmspace.h>

#include "USB_Serial.h"
#include "UART_Serial.h"

#include "../pc_interface.h"

/** LUFA CDC Class driver interface configuration and state information. This structure is
 *  passed to all CDC Class driver functions, so that multiple instances of the same class
 *  within a device can be differentiated from one another.
 */
USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface = {
    .Config =
    {
        .ControlInterfaceNumber         = INTERFACE_ID_CDC_CCI,
        .DataINEndpoint                 =
        {
            .Address                = CDC_TX_EPADDR,
            .Size                   = CDC_TXRX_EPSIZE,
            .Banks                  = 1,
        },
        .DataOUTEndpoint                =
        {
            .Address                = CDC_RX_EPADDR,
            .Size                   = CDC_TXRX_EPSIZE,
            .Banks                  = 1,
        },
        .NotificationEndpoint           =
        {
            .Address                = CDC_NOTIFICATION_EPADDR,
            .Size                   = CDC_NOTIFICATION_EPSIZE,
            .Banks                  = 1,
        },
    },
};

/** Configures the board hardware and chip peripherals for the application's functionality. */
void USB_SetupHardware(void)
{
    /* Disable watchdog if enabled by bootloader/fuses */
    MCUSR &= ~(1 << WDRF);
    wdt_disable();

    /* Disable clock division */
    clock_prescale_set(clock_div_1);

    /* Hardware Initialization */
    USB_Init();
}

/**
 * Initializes the stdio for USB CDC communication
 */
void USB_SetupSoftware(void)
{
    usbFile = fdevopen(USB_put, USB_get);
    usbConnected = false;
}

/** Event handler for the library USB Connection event. */
void EVENT_USB_Device_Connect(void)
{
    fputs_P(PSTR("USB Connected\r\n"), uartOne);
    usbConnected = true;
    stdout = usbFile;
}

/** Event handler for the library USB Disconnection event. */
void EVENT_USB_Device_Disconnect(void)
{
    /* Only print when USB was connected prior to this being called.
     * This is called at start up if no USB connect is available */
    if(usbConnected) {
        fputs_P(PSTR("USB Disconnected\r\n"), uartOne);
    }

    usbConnected = false;
    stdout = uartOne;
}

/** Event handler for the library USB Configuration Changed event. */
void EVENT_USB_Device_ConfigurationChanged(void)
{
    bool ConfigSuccess = true;

    ConfigSuccess &= CDC_Device_ConfigureEndpoints(&VirtualSerial_CDC_Interface);

    if(ConfigSuccess) {
        fputs_P(PSTR("USB Config Changed Successfully\r\n"), uartOne);
    } else {
        fputs_P(PSTR("USB Config Change Failed\r\n"), uartOne);
    }
}

/** Event handler for the library USB Control Request reception event. */
void EVENT_USB_Device_ControlRequest(void)
{
    CDC_Device_ProcessControlRequest(&VirtualSerial_CDC_Interface);
}

/**
 * Returns a character received over USB CDC
 *
 * Just a front for CDC_Device_ReceiveByte().
 *
 * @param unused Unused
 * @returns Character received if available, otherwise -1
 */
int USB_get(FILE *unused)
{
    return CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
}

/**
 * Sends the given char out over USB CDC
 *
 * @param outChar Char to send
 * @param unused Unused
 *
 * @return 0 if transmit was successful, non zero value if it was unsuccessful
 */
int USB_put(char outChar, FILE *unused)
{
    uint8_t result = CDC_Device_SendByte(&VirtualSerial_CDC_Interface,
                                         outChar);

    if(result == ENDPOINT_READYWAIT_NoError) {
        return 0;
    } else {
        return result;
    }
}

void USB_main_loop(void)
{

    if(usbConnected) {
        /* Store received byte into the USART transmit buffer */
        while(CDC_Device_BytesReceived(&VirtualSerial_CDC_Interface) > 0) {
            int receivedByte = USB_get(NULL);

            if(receivedByte >= 0) {

                buffer_add_to(receivedByte);
            }
        }

        CDC_Device_USBTask(&VirtualSerial_CDC_Interface);
        USB_USBTask();
    }
}