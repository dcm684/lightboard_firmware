/*
 * EEPROM_25AA640A.h
 *
 * Created: 8/17/2015 12:35:30 PM
 *  Author: Meyer
 */

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <avr/io.h>


#ifndef EEPROM_25AA640A_H_
#define EEPROM_25AA640A_H_

/**
 * @def EEPROM_PORT
 * Port attached to EEPROM chip
 */
#define EEPROM_PORT PORTB

/**
 * @def EEPROM_DDR
 * DDR of port attached to EEPROM chip
 */
#define EEPROM_DDR DDRB

/**
 * @def EEPROM_CS
 * Pin attached to Chip Select of EEPROM chip
 */
#define EEPROM_CS PORTB4

/**
 * @def EEPROM_HOLD
 * Pins attached to HOLD pin on EEPROM chip
 */
#define EEPROM_HOLD PORTB5

/**
 * @def EEPROM_WP
 * Pin attached to write protect pin on EEPROM chip
 */
#define EEPROM_WP PORTB6

/**
 * @def EEPROM_INST_READ
 * Command sent to EEPROM when requesting to read starting at a given byte
 */
#define EEPROM_INST_READ 0x03

/**
 * @def EEPROM_INST_WRITE
 * Command sent to EEPROM when requesting to write starting at a given byte
 */
#define EEPROM_INST_WRITE 0x02

/**
 * @def EEPROM_INST_DISABLE_WRITE
 * Command sent to EEPROM when requesting to disable write operations by
 * resetting the write enable latch
 */
#define EEPROM_INST_DISABLE_WRITE 0x04

/**
 * @def EEPROM_INST_ENABLE_WRITE
 * Command sent to EEPROM when requesting to enable write operations by
 * setting the write enable latch
 */
#define EEPROM_INST_ENABLE_WRITE 0x06

/**
 * @def EEPROM_INST_STATUS_READ
 * Command sent to EEPROM when requesting to read the status register
 */
#define EEPROM_INST_STATUS_READ 0x05

/**
 * @def EEPROM_INST_STATUS_WRITE
 * Command sent to EEPROM when requesting to write to the status register
 */
#define EEPROM_INST_STATUS_WRITE 0x01

/**
 * @def EEPROM_CAPACITY
 * Number of bytes available to store data in the EEPROM
 */
#define EEPROM_CAPACITY 8192

/**
 * @def EEPROM_BYTES_PER_PAGE
 * Number of bytes available per EEPROM page
 */
#define EEPROM_BYTES_PER_PAGE 32

void eeprom_25aa640a_set_active(void);
void eeprom_25aa640a_set_inactive(void);

void eeprom_25aa640a_init(void);
void eeprom_25aa640a_set_wren(bool enable);

uint8_t eeprom_25aa640a_read_status(void);
void eeprom_25aa640a_hold_while_wip(void);
void eeprom_25aa640a_write_status(uint8_t status);

uint8_t eeprom_25aa640a_read_byte(const uint16_t *__p);
size_t eeprom_25aa640a_read_block(void *__dst, const void *__src, size_t __n);

void eeprom_25aa640a_write_byte(uint16_t *__p, uint8_t __value);
size_t eeprom_25aa640a_write_block(const void *__src, void *__dst, size_t __n);


#endif /* EEPROM_25AA640A_H_ */