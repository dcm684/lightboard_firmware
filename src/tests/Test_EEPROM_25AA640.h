/*
 * Test_EEPROM_25AA640.h
 *
 * Created: 8/19/2015 1:52:02 PM
 *  Author: Meyer
 */


#ifndef TEST_EEPROM_25AA640_H_
#define TEST_EEPROM_25AA640_H_

void test_byte_write_read(void);

void test_array_print(uint8_t *block, size_t blockSize);
void test_block_write_read(void);
#endif /* TEST_EEPROM_25AA640_H_ */