/*
 * Test_EEPROM_25AA640.c
 *
 * Created: 8/19/2015 1:51:41 PM
 *  Author: Meyer
 */

#include <avr/pgmspace.h>

#include "Test_EEPROM_25AA640.h"

#include "../display.h"
#include "../pc_interface.h"
#include "../drivers/EEPROM_25AA640A.h"
#include "../main.h"

#include "../drivers/SPI.h"
#include "../drivers/UART_Serial.h"

/**
 * Writes a byte to the EEPROM and then reads it
 */
void test_byte_write_read(void)
{
    uint16_t *dataAddress = 0x0000;
    const int8_t statusReadDelay = 5; /* Time to wait between setting the
										EEPROM status byte and reading the
										byte */

    puts_P(PSTR("Start\r"));

    puts_P(PSTR("Toggle Active\r"));
    eeprom_25aa640a_set_active();
    _delay_ms(statusReadDelay);
    eeprom_25aa640a_set_inactive();

    puts_P(PSTR("\nWREN OFF\r"));
    eeprom_25aa640a_set_wren(false);
    _delay_ms(statusReadDelay);
    printf_P(PSTR("Status (0x80): 0x%02X\r\n"), eeprom_25aa640a_read_status());

    puts_P(PSTR("WREN ON\r"));
    eeprom_25aa640a_set_wren(true);
    _delay_ms(statusReadDelay);
    printf_P(PSTR("Status (0x82): 0x%02X\r\n"), eeprom_25aa640a_read_status());

    puts_P(PSTR("\nWPEN On\r"));
    eeprom_25aa640a_write_status(0x80);
    _delay_ms(statusReadDelay);
    printf_P(PSTR("Status (0x80): 0x%02X\r\n"), eeprom_25aa640a_read_status());

    puts_P(PSTR("\nClear Byte\r"));
    eeprom_25aa640a_write_byte(dataAddress, 0x00);
    printf_P(PSTR("Read data (0x00): 0x%02X\r\n"),
             eeprom_25aa640a_read_byte(dataAddress));

    puts_P(PSTR("\nWrite Byte\r"));
    eeprom_25aa640a_write_byte(dataAddress, 0xF0);
    printf_P(PSTR("Read data (0xF0): 0x%02X\r\n"),
             eeprom_25aa640a_read_byte(dataAddress));

    puts_P(PSTR("Done\r\n"));
}

/**
 * Prints an array
 *
 * @param block Array to print
 * @param blockSize Size of array
 */
void test_array_print(uint8_t *block, size_t blockSize)
{
    uint8_t i;

    printf_P(PSTR("["));

    for(i = 0; i < blockSize; i++) {
        printf_P(PSTR("%X"), block[i]);

        if(i < blockSize - 1) {
            fputs_P(PSTR(", "), uartOne);
        } else {
            fputs_P(PSTR("]\r\n"), uartOne);
        }
    }
}

/**
 * Writes a block to the EEPROM and reads it
 *
 * All values are printed and it is the responsibility of the user
 * to verify everything
 */
void test_block_write_read(void)
{
    uint16_t dataAddress = 0x0000;
    uint16_t blockTestLength = 100; /* Number of addressed to use
												in block read/write test */

    uint8_t blockTestBlock[blockTestLength]; /* Array to use in block
												read/write test */
    size_t blockTestRetVal; /* Value returned during block reads and
								writes */

    uint8_t i, j;

    /* Test the read and writes with valid parameters */
    for(i = 0; i < 2; i++) {
        if(i == 0) {
            /* Clear the block */
            puts_P(PSTR("\nClear Block\r"));

            for(j = 0; j < blockTestLength; j++) {
                blockTestBlock[j] = 0;
            }
        } else {
            /* Add values to the block */
            printf_P(PSTR("\n0, 1, ..., %X, %X Block\r\n"),
                     blockTestLength - 2,
                     blockTestLength - 1);

            for(j = 0; j < blockTestLength; j++) {
                blockTestBlock[j] = j;
            }
        }

        /* Write the values */
        blockTestRetVal = eeprom_25aa640a_write_block(blockTestBlock,
                          (void *) dataAddress,
                          blockTestLength);

        printf_P(PSTR("Bytes Written: %u\r\n"), blockTestRetVal);

        /*  Read cleared block */
        blockTestRetVal = eeprom_25aa640a_read_block(blockTestBlock,
                          (void *) dataAddress,
                          blockTestLength);

        printf_P(PSTR("Bytes Read: %u\r\n"), blockTestRetVal);

        /* Print the block */
        test_array_print(blockTestBlock, blockTestLength);
    }

    /* Test the functions with invalid parameters */
    puts_P(PSTR("\nTest read with start out of range\r"));
    dataAddress = EEPROM_CAPACITY;
    blockTestLength = 1;
    blockTestRetVal = eeprom_25aa640a_read_block(blockTestBlock,
                      (void *) dataAddress,
                      blockTestLength);
    printf_P(PSTR("Address: %04X\tLen: %04X\t Result(0): %X\r\n"),
             dataAddress,
             blockTestLength,
             blockTestRetVal
            );

    puts_P(PSTR("\nTest read with start in range, but too long\r"));
    dataAddress = EEPROM_CAPACITY - 1;
    blockTestLength = 2;
    blockTestRetVal = eeprom_25aa640a_read_block(blockTestBlock,
                      (void *) dataAddress,
                      blockTestLength);
    printf_P(PSTR("Address: %04X\tLen: %04X\t Result(1): %X\r\n"),
             dataAddress,
             blockTestLength,
             blockTestRetVal
            );

    puts_P(PSTR("\nTest write with start out of range\r"));
    dataAddress = EEPROM_CAPACITY;
    blockTestLength = 1;
    blockTestRetVal = eeprom_25aa640a_write_block(blockTestBlock,
                      (void *) dataAddress,
                      blockTestLength);
    printf_P(PSTR("Address: %04X\tLen: %04X\t Result(0): %X\r\n"),
             dataAddress,
             blockTestLength,
             blockTestRetVal
            );

    puts_P(PSTR("\nTest write with start in range, but too long\r"));
    dataAddress = EEPROM_CAPACITY - 1;
    blockTestLength = 2;
    blockTestRetVal = eeprom_25aa640a_write_block(blockTestBlock,
                      (void *) dataAddress,
                      blockTestLength);
    printf_P(PSTR("Address: %04X\tLen: %04X\t Result(1): %X\r\n"),
             dataAddress,
             blockTestLength,
             blockTestRetVal
            );
}