#ifndef DISPLAY_H
#define DISPLAY_H

#include "main.h"

/**
 *This file contains the defines used in driving the board
 */

#define DISPLAY_ROW_ON 1

/* Pin defines */
#if 1 /* ATMEGA32U2 */
#define DISPLAY_PORT PORTD
#define DISPLAY_DDR DDRD

#define D_DATA1 PORTD4
#define D_DATA2 PORTD5
#define D_DATA3 PORTD6
#define D_STROBE PORTD0
#define D_OE PORTD1
#define D_CLOCK PORTD7

#define DDR_DATA1 DDD4
#define DDR_DATA2 DDD5
#define DDR_DATA3 DDD6
#define DDR_STROBE DDD0
#define DDR_OE DDD1
#define DDR_CLOCK DDD7

#define SIPO1_OFF  0b0000000
#define SIPO2_OFF  0b0001110
#define SIPO3_OFF  0b1111111

#else /* ATMEGA64 */
#define DISPLAY_PORT PORTA
#define DISPLAY_DDR DDRA

#define D_DATA1 PA0
#define D_DATA2 PA1
#define D_DATA3 PA2
#define D_STROBE PA3
#define D_OE PA4
#define D_CLOCK PA5

#define DDR_DATA1 DDA0
#define DDR_DATA2 DDA1
#define DDR_DATA3 DDA2
#define DDR_STROBE DDA3
#define DDR_OE DDA4
#define DDR_CLOCK DDA5

#define HEART_PORT PORTD
#define HEART_DDR DDRD
#define HEART PD7
#define DDR_HEART DDD7

#define SIPO1_OFF  0b0000000
#define SIPO2_OFF  0b0001110
#define SIPO3_OFF  0b1111111
#endif

/**
 * @def INT0_PER_COL
 * Number of Timer 0 interrupts that should pass before advancing to the
 * next column
 */
#define INT0_PER_COL 1

volatile bool bufferNextScreen; /**<  True, if the next screen should be buffered */
volatile uint8_t columnIterator; /**< Current column in a screen */
volatile uint8_t delayIterator; /**< Current position in screen delay counter */
volatile uint8_t currentDelay; /**< How long the current screen is displayed */

volatile uint8 displayScreenCounter; /**< Tracks how long a column has been displayed */

void displayInterruptHandler(void);
void initDisplay(void);
void writeDataOut(uint8, uint8, uint8);
void drawAColumn(uint16[10], uint8);
//void writeBoard(uint16[10]);
//void animateBoard(uint16[][10], uint8[], uint8);
void displayCycleLEDs(int8 startLED, int8 stopLED);
void disableDisplayInt(void);
void enableDisplayInt(void);
void bufferScreen(void);
void displayRestart(void);

#endif /* DISPLAY_H */
