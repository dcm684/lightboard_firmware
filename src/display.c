#include "main.h"
#include "display.h"
#include "data_storage.h"

extern volatile bool dataInRXBuffer;

static bool displayRepaintEnabled = true;

//One delay value is 10ms
uint16_t screenBuffer[10];

/*
uint16_t allOnScreen[10] = {0x3FF, 0x3FF, 0x3FF, 0x3FF, 0x3FF,
	0x3FF, 0x3FF, 0x3FF, 0x3FF, 0x3FF};
*/

/**
 * Updates the display
 *
 * To be called once every millisecond
 */
void displayInterruptHandler(void)
{
    if(!displayRepaintEnabled) {
        return;
    }

    displayScreenCounter++;

    if(displayScreenCounter == INT0_PER_COL) {
        /* Print one column per interrupt */
        drawAColumn(screenBuffer, columnIterator);

        columnIterator++;

        if(columnIterator >= 10) {
            columnIterator = 0;
            delayIterator++;

            if(delayIterator >= currentDelay) {
                delayIterator = 0;
                
                bufferNextScreen = true;
            }
        }

        displayScreenCounter = 0;
    }
}

/**
 * Initialize all display related variables
 */
void initDisplay()
{
    DISPLAY_DDR |= (1U << DDR_DATA1);
    DISPLAY_DDR |= (1U << DDR_DATA2);
    DISPLAY_DDR |= (1U << DDR_DATA3);
    DISPLAY_DDR |= (1U << DDR_STROBE);
    DISPLAY_DDR |= (1U << DDR_OE);
    DISPLAY_DDR |= (1U << DDR_CLOCK);

    HEART_DDR |= (1U << DDR_HEART);

    DISPLAY_PORT &= ~_BV(D_STROBE); /* Hold strobe low*/
    DISPLAY_PORT |= _BV(D_OE); /* Toggle OE high */
    DISPLAY_PORT |= _BV(D_CLOCK); /* Toggle Clock high */

    writeDataOut(SIPO1_OFF, SIPO2_OFF, SIPO3_OFF);

    displayRestart();
}

/**
 * Shifts one line of the display out of the three serial pins
 *
 * Bits are sent out MSb first. Last bit ought to go to QP0 on the
 * shift register
 *
 * The LSB of each input is sent out first.
 *
 * @param SIPOOneIn Byte to push out to shift register 1
 * @param SIPOTwoIn Byte to push out to shift register 2
 * @param SIPOThreeIn Byte to push out to shift register 3
 */
//#define REVERSED_WRITE_DATA_OUT
void writeDataOut(uint8 SIPOOneIn, uint8 SIPOTwoIn, uint8 SIPOThreeIn)
{
    uint8_t i;

    uint8_t shiftSize;

    for(i = 0; i < 7; i++) {
#ifdef REVERSED_WRITE_DATA_OUT
        shiftSize = i;
#else
        shiftSize = 6 - i;
#endif /* REVERSED_WRITE_DATA_OUT */

        /* Set bit for SIPO chip one */
        if((SIPOOneIn >> shiftSize) & 0x01) {
            DISPLAY_PORT |= _BV(D_DATA1);
        } else {
            DISPLAY_PORT &= ~_BV(D_DATA1);
        }

        /* Set bit for SIPO chip two */
        if((SIPOTwoIn >> shiftSize) & 0x01) {
            DISPLAY_PORT |= _BV(D_DATA2);
        } else {
            DISPLAY_PORT &= ~_BV(D_DATA2);
        }

        /* Set bit for SIPO chip three */
        if((SIPOThreeIn >> shiftSize) & 0x01) {
            DISPLAY_PORT |= _BV(D_DATA3);
        } else {
            DISPLAY_PORT &= ~_BV(D_DATA3);
        }


        /* Clock data through the SIPO */
        DISPLAY_PORT &= ~_BV(D_CLOCK); /* Toggle clock low */
        DISPLAY_PORT |= _BV(D_CLOCK); /* Toggle clock high */
    }

    /* Output data stored in SIPO */
    DISPLAY_PORT |= _BV(D_STROBE); /* Load display registers on SIPOs */
    DISPLAY_PORT &= ~_BV(D_STROBE); /* Bring strobe back down */

}


/**
 * Draws the given column in the given screen
 *
 * @param LEDArray Screen to draw
 * @param colNum Column out screen to draw
 */
void drawAColumn(uint16 LEDArray[10], uint8 colNum)
{
    uint8 j;

    uint16 SIPOOne;
    uint16 SIPOTwo;
    uint16 SIPOThree;

    uint16 tempInput;
    uint16 arrayDump;

    SIPOOne = 0;
    SIPOTwo = 0;
    SIPOThree = 0;

    tempInput = 0;

    /* Don't take an invalid column number. If one is received don't
    modify the display */
    if(colNum > 9) {
        return;
    }

    /* Load each row state for the column */
    tempInput = 0;

    for(j = 0; j < 10; j++) {
        /* Load one row */
        arrayDump = LEDArray[j];
        /* Get state for LED in given column for that row */
        arrayDump = arrayDump & (1 << colNum);
        /* Move state from col position to row position */
        arrayDump = (arrayDump >> colNum) << j;
        /* Add it to the column temp */
        tempInput = tempInput | arrayDump;
    }

#ifdef REVERSED_WRITE_DATA_OUT

    /* Set SIPOOne to draw the desired column */
    SIPOOne = (1 << (9 - colNum));

    /* Reverse tempInput and put it in SIPO3 Temp Input = ABCDEFGHIJ */
    SIPOThree = tempInput >> 9; /* SIPO3 = 000000000A */
    SIPOThree = SIPOThree | ((tempInput >> 7) & (1 << 1)); /* S3 = 00000000BA */
    SIPOThree = SIPOThree | ((tempInput >> 5) & (1 << 2)); /* S3 = 0000000CBA */
    SIPOThree = SIPOThree | ((tempInput >> 3) & (1 << 3)); /* S3 = 000000DCBA */
    SIPOThree = SIPOThree | ((tempInput >> 1) & (1 << 4)); /* S3 = 00000EDCBA  */
    SIPOThree = SIPOThree | ((tempInput << 1) & (1 << 5)); /* S3 = 0000FEDCBA  */
    SIPOThree = SIPOThree | ((tempInput << 3) & (1 << 6)); /* S3 = 000GFEDCBA  */
    SIPOThree = SIPOThree | ((tempInput << 5) & (1 << 7)); /* S3 = 00HGFEDCBA  */
    SIPOThree = SIPOThree | ((tempInput << 7) & (1 << 8)); /* S3 = 0IHGFEDCBA  */
    SIPOThree = SIPOThree | ((tempInput << 9) & (1 << 9)); /* S3 = JIHGFEDCBA  */
#else
    SIPOOne = 1 << colNum;
    SIPOThree = tempInput;
#endif /* REVERSED_WRITE_DATA_OUT */

    /* Ensure only the reverse column is loaded */
    SIPOThree = SIPOThree & 0x3FF;

    //printf("%2X\r\n", SIPOThree);

    /* Set SIPOThree to the state required to drive the rows */
#if DISPLAY_ROW_ON == 0
    SIPOThree = ~SIPOThree;
#endif /* DISPLAY_ROW_ON */
    SIPOOne = ~SIPOOne;

    /* Load the streams to be sent to the SIPOs. (Remember SIPOOne already has
    the column number */
#ifdef REVERSED_WRITE_DATA_OUT
    SIPOTwo = ((SIPOOne & 0x07) << 4) | ((SIPOThree >> 6) & 0x0F);
    SIPOOne = SIPOOne >> 3;
    SIPOThree = SIPOThree & 0x7F;
#else
    SIPOTwo = ((SIPOThree & 0x07) << 3) | ((SIPOOne >> 7) & 0x07);
    SIPOOne &= 0x7F;
    SIPOThree = (SIPOThree >> 3) & 0x7F;
#endif /* REVERSED_WRITE_DATA_OUT */

    writeDataOut((uint8) SIPOOne, (uint8) SIPOTwo, (uint8) SIPOThree);
    //_delay_us(1);
}

/* Write a 10x10 array to the board */
/*
void writeBoard(uint16 LEDArray[10])
{
	uint8 i;

	for (i = 0; i < 10; i++) {

		#ifdef ROW_METHOD

		tempInput = LEDArray[i];

		SIPOOne = tempInput >> 9;
		SIPOOne = SIPOOne | ((tempInput >> 7) & (1 << 1));
		SIPOOne = SIPOOne | ((tempInput >> 5) & (1 << 2));
		SIPOOne = SIPOOne | ((tempInput >> 3) & (1 << 3));
		SIPOOne = SIPOOne | ((tempInput >> 1) & (1 << 4));
		SIPOOne = SIPOOne | ((tempInput << 1) & (1 << 5));
		SIPOOne = SIPOOne | ((tempInput << 3) & (1 << 6));
		SIPOOne = SIPOOne | ((tempInput << 5) & (1 << 7));
		SIPOOne = SIPOOne | ((tempInput << 7) & (1 << 8));
		SIPOOne = SIPOOne | ((tempInput << 9) & (1 << 9));

		SIPOThree = (1 << (9 - i))^(0x3FF);
		SIPOTwo = ((SIPOOne & 0x07) << 4) | ((SIPOThree >> 6) & 0x0F);
		SIPOOne = SIPOOne >> 3;
		SIPOThree = SIPOThree & 0x7F;

		#endif

		drawAColumn(LEDArray, i);

		//Stop updating since the data in the buffer is more important
		if(dataInRXBuffer){
			break;
		}
	}
}
*/


/* Cycles through all of the board patterns */
/*
void animateBoard(uint16 arrayOfDisplays[][10], uint8 arrayOfDelays[], uint8 lengthOfArray)
{
	uint8 i;
	uint8 delayCounter;

	for ( i = 0; i < lengthOfArray; i ++ ) {
		for ( delayCounter = 0; delayCounter < arrayOfDelays[i]; delayCounter++ ) {
			writeBoard(arrayOfDisplays[i]);
			if(dataInRXBuffer){
				break;
			}
		}
		//printf("\r\n%u/%u: %u", i, lengthOfArray, arrayOfDelays[i]);
		if(dataInRXBuffer){
			break;
		}
	}
}
*/

/**
 * Loads the next screen into a 10 by 10bit buffer
 *
 * When the entire screen array is stored, each screen is a 12 by 8bit array.
 * This function fills a buffer made of 10, 16bit  indices. Only 10 of those
 * 16bits is used.
 *
 * @param inArray Screen to load into screen buffer
 */
void bufferScreen(void)
{
    uint8_t inArray[EE_PATERN_SIZE];
    
    readNextScreenFromEEPROM(inArray, (uint8_t *) &currentDelay);

    screenBuffer[0] = (inArray[1] << 8) | (inArray[0]);
    screenBuffer[1] = (inArray[2] << 6) | (inArray[1] >> 2);
    screenBuffer[2] = (inArray[3] << 4) | (inArray[2] >> 4);
    screenBuffer[3] = (inArray[4] << 2) | (inArray[3] >> 6);

    screenBuffer[4] = (inArray[6] << 8) | (inArray[5]);
    screenBuffer[5] = (inArray[7] << 6) | (inArray[6] >> 2);
    screenBuffer[6] = (inArray[8] << 4) | (inArray[7] >> 4);
    screenBuffer[7] = (inArray[9] << 2) | (inArray[8] >> 6);

    screenBuffer[8] = (inArray[11] << 8) | (inArray[10]);
    screenBuffer[9] = (inArray[12] << 6) | (inArray[11] >> 2);
}

#if 0
/**
 * Populates outputArray to illuminate a series of LEDs sequentially
 *
 * @param startLED First LED to illuminate
 *   startLED >= 0
 *   startLED < 98
 * @param stopLED First LED to not illuminate
 *   startLED < stopLED
 *   stopLED < startLED + MAX_LENGTH
 *   stopLED <= 100
 *   If either requirement is invalid, stopLED will be set to the lesser of
 *     startLED + MAX_LENGTH - 1 or 99
 */
void displayCycleLEDs(int8 startLED, int8 stopLED)
{
    uint8_t i;
    uint8_t j;

    uint8_t activeLEDs = DISPLAY_MAX_SCREENS;
    const uint8_t startBit = 0; /* First bit in byte */
    const uint8_t stopBit = 7; /* Bits per byte - 1*/
    const uint8_t fixedDelay = 10;

    /* Let there be some animation, even if the start LED is out of range */
    if(startLED > 98) {
        startLED = 98;
    }

    /* Make sure stop LED is within constraints and if it is not put
    it there */
    if((stopLED < startLED)
            || (stopLED > 100)
            || (stopLED - startLED > DISPLAY_MAX_SCREENS)) {

        if(100 - DISPLAY_MAX_SCREENS < startLED) {
            stopLED = 100;
        } else {
            stopLED = startLED + DISPLAY_MAX_SCREENS;
        }
    }

    activeLEDs = stopLED - startLED;

    /* Clear out the old data */
    for(i = 0; i < activeLEDs; i++) {
        for(j = 0; j < 13; j++) {
            outputArray[i][j] = 0;
        }
    }

    uint8_t currByte;
    uint8_t currBit;

    currByte = startLED / 8;
    currBit = startLED % 8;

    for(i = 0; i < activeLEDs; i++) {
        /* No need to clear last LED since entire array was was cleared and
        we are updating that array */

        /* Set the current LED */
        outputArray[i][currByte] = _BV(currBit);

        /* Set the timing array */
        delayArray[i] = fixedDelay;

        /* Select the next bit and byte */
        currBit++;

        if(currBit > stopBit) {
            currBit = startBit;
            currByte++;
        }
    }

    /* Set the delay for the final blank screen */
    delayArray[DISPLAY_MAX_SCREENS - 1] = fixedDelay;

    /* Clear all on last screen */
    //outputArray[MAX_LENGTH][currByte] &= ~_BV(currBit);

    outputArrayLength = activeLEDs;
}
#endif

/**
 * Disables the light board repaint during the timer 0 interrupt
 */
void disableDisplayInt(void)
{
    displayRepaintEnabled = false;
}

/**
 * Re-enables the light board repaint during the timer 0 interrupt
 */
void enableDisplayInt(void)
{
    displayRepaintEnabled = true;
}

/**
 * Restarts the pattern from the beginning
 */
void displayRestart(void)
{
    columnIterator = 0;
    delayIterator = 0;
    displayScreenCounter = 0;

    bufferNextScreen = true;
}