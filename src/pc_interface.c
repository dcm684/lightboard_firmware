#include <progmem.h>

#include "pc_interface.h"
#include "display.h"
#include "data_storage.h"

#include "drivers/UART_Serial.h"
#include "drivers/USB_Serial.h"

extern void clearScreen(void);

/**
 * Determines the next action to take based upon the char at the front of
 * the RX buffer
 */
void serial_buffer_parser(void)
{
    char inChar;

    while(!buffer_is_empty()) {

        /* Read the next unread byte */
        inChar = buffer_get_next();

        //printf("\r\n%u %u ", rxBufferCurrentRead, rxBufferIndex);
        if(inChar == SERIAL_READY_CHAR) {
            /* Download a new display */
            fputc(inChar, stdout); /* Echo ready char */
            downloadNewBoard();

        } else if(inChar == SERIAL_CLEAR_EEPROM) {
            /* Flush the EEPROM and reload the default values */
            fputc(inChar, stdout); /* Indicate clearing has started */
            clearScreen();
            fputc(inChar, stdout); /* Indicate clearing has been completed */
        }

    }

    dataInRXBuffer = false;
}

/**
 * Initializes the serial circular buffer
 */
void buffer_init(void)
{
    serial_rx_buffer.count = 0;
    serial_rx_buffer.front = 0;
    serial_rx_buffer.data[0] = 0;

    dataInRXBuffer = false;
}

/**
 * Adds value to buffer
 *
 * Value will not be added if buffer is full
 *
 * @param toAdd Value to add to the buffer
 * @returns Number of items in buffer
 */
uint8_t buffer_add_to(char toAdd)
{
    if(!buffer_is_full()) {
        /* Wrap around if there is no room to put new item at end of
         * buffer */
        uint8_t index;

        if(serial_rx_buffer.front >= UART_RX_BUFFER_SIZE - serial_rx_buffer.count) {
            index = serial_rx_buffer.count - (UART_RX_BUFFER_SIZE
                                              - serial_rx_buffer.front);
        } else {
            index = serial_rx_buffer.front + serial_rx_buffer.count;
        }

        /* Add the data at the next available spot */
        serial_rx_buffer.data[index] = toAdd;

        /* Incrementing counter after adding removes need for decrementing
         * in index finding */
        serial_rx_buffer.count++;
    }

    dataInRXBuffer = true;

    return serial_rx_buffer.count;
}

/**
 * If one is available, return the next unread item in the RX buffer
 *
 * @return Next unread item in RX buffer. 0 if buffer is empty
 */
char buffer_get_next(void)
{
    char outChar = 0;

    if(!buffer_is_empty()) {
        outChar = serial_rx_buffer.data[serial_rx_buffer.front];

        /* Determine the new front. If we are at the end of the buffer
         * wrap around */
        if(serial_rx_buffer.front == UART_RX_BUFFER_SIZE - 1) {
            serial_rx_buffer.front = 0;
        } else {
            serial_rx_buffer.front++;
        }

        /* There's one fewer item in the buffer */
        serial_rx_buffer.count--;
    }

    return outChar;
}

/**
 * Is the RX buffer empty?
 *
 * @returns True, if RX buffer is empty
 */
bool buffer_is_empty(void)
{
    return (serial_rx_buffer.count == 0);
}

/**
 * Is the buffer full?
 *
 * @returns True if the buffer is full
 */
bool buffer_is_full(void)
{
    return (serial_rx_buffer.count == UART_RX_BUFFER_SIZE);
}

/**
 * Handles the receipt of a new series of screens to display
 *
 * If the receipt fails the last stored board is loaded from EEPROM. If
 * nothing was stored in EEPROM, garbage may be loaded. The original
 * board length will remain.
 * 
 * Expected communication
 * IN:  SEND_SERIAL_CHAR (Handled by buffer parser)
 * OUT: SEND_SERIAL_CHAR (Handled by buffer parser)
 * IN:  Number of patterns in display
 * OUT: Number of patterns in display (0 if value received in invalid)
 * IN:  Screen 0: Pattern Byte 0, ..., Pattern Byte 12, Delay Byte, Checksum
 * Out: Checksum (All bytes for screen 0 XOR'd, including received checksum)
 *          Value should be 0 if valid
 * ...
 * IN: Screen X
 * OUT: Screen X checksum
 */
void downloadNewBoard()
{
    char inChar;
    uint16_t screenCounter = 0;
    uint8_t screenByteCounter = 0;
    uint8_t checkSum = 0;

    uint8_t receivedLength = 0;

    uint32 timeoutTime;
    bool timedOut = false;
    
    uint8_t pattern[13]; /* @TODO: Make 13 a define */
    uint8_t delay = 0;
    
    /* Send the maximum number of patterns allowed (2 bytes, MSB first) */
    fprintf_P(stdout, PSTR("%c%c"),
                (EE_BANK_SCREENS >> 8) & 0xFF, 
                (EE_BANK_SCREENS) & 0xFF);

    /* Stop repainting the display */
    disableDisplayInt();

    timeoutTime = systemTime + TIME_OUT_MS;

    while(buffer_is_empty() && (timeoutTime > systemTime)) {
        USB_main_loop();
    }

    /* Stop receiving data and leave if the length is never received */
    if(buffer_is_empty()) {
        /* Don't print errors on UART when its the only interface
         * with the PC */
        if(usbConnected) {
            fputs_P(PSTR("Length timed out\r\n"), uartOne);
        }

        timedOut = true;

    } else {
        /* Get the array length */
        receivedLength = buffer_get_next();
    }

    /* Only proceed if a valid length is received */
    if((0 < receivedLength) && (receivedLength < EE_BANK_SCREENS)) {
        
        /* Acknowledge the received length to the sender */
        fputc(receivedLength, stdout);
        
        /* Receive each expected screen */
        for(screenCounter = 0; screenCounter < receivedLength; screenCounter++) {
            
            /* Each screen has one checksum all bytes including delay are
             * XORd together and returned to sender */
            checkSum = 0;
            
            /* Received each byte of the received screen */
            /* @TODO: Make 14 a define of screen byte count + 1 for delay + 1 for CS */
            for(screenByteCounter = 0; screenByteCounter < 15; screenByteCounter++) {

                /* Wait for the next char to come in. USB must be
                 * continuously checked for incoming data */
                timeoutTime = systemTime + TIME_OUT_MS;

                while(buffer_is_empty() && (timeoutTime > systemTime)) {
                    USB_main_loop();
                }

                /* Exit if timed out */
                if(buffer_is_empty()) {
                    if(usbConnected) {
                        fprintf_P(uartOne,
                                  PSTR("Screen %u, byte %u timed out\r\n"),
                                  screenCounter, screenByteCounter);
                    }

                    timedOut = true;

                    break;
                }

                /* Read in the next received byte and add it to 
                 * the checksum. If the entire pattern has been received,
                 * store the pattern and delay in EEPROM */
                inChar = buffer_get_next();
                checkSum = checkSum ^ inChar;
                
                if (screenByteCounter < 13) {
                    pattern[screenByteCounter] = inChar;
                    
                } else if (screenByteCounter == 13) {
                    delay = inChar;
                    
                } else {
                    /* Verify the CS which should be 0 since the last byte 
                     * is the CS and it was XORd against the calculated CS.
                     * If the CS is valid store the screen in EEPROM */
                    if (checkSum == 0) {
                        storeNextScreenInEEPROM(receivedLength,
                                                pattern,
                                                delay);
                    }
                    
                    /* Return a checksum to the computer sending the 
                     * commands after receiving all of the data for a 
                     * given screen */
                    fputc(checkSum, stdout);
                }
            }

            if(timedOut) {
                break;
            }
        }
        
    } else {
        /* Indicate to the sender that the received length was invalid */
        fputc(0, stdout);
    }
    
    /* If the message was successfully received, change the active display
     * to the current pattern, and make the pattern to overwrite the 
     * pattern that was displayed prior to the download
     * 
     * @TODO: Switch banks
     */
    if (!timedOut) {
    }

    displayRestart();

    /* Start updating the display again */
    enableDisplayInt();
}

/**
 * Prints the bytes for the given screen on the hardware UART
 * 
 * Format is:
 * (Delay: 000) 0x00 0x11 ... 0xXX
 * 
 * @param pattern Pattern to display
 * @param delay Delay to display
 */
void print_a_screen(uint8_t pattern[13], uint8_t delay)
{
    uint8_t i;
    fprintf_P(uartOne, PSTR("(Delay: %3u)\t"), delay);
    
    for(i = 0; i < 13; i++) {
        fprintf_P(uartOne, PSTR("%02X "), pattern[i]);
    }
    
    fputs_P(PSTR("\r\n"), uartOne);
}