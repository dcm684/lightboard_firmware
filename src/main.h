#ifndef MAIN_H
#define MAIN_H

#ifndef F_CPU
#define F_CPU 8000000UL /* Defined in project settings */
#endif

#include <inttypes.h>
#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// Define some useful types:
typedef signed char int8;
typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;

//#define nop()  __asm__ __volatile__("nop")

/**
 * @def TCNT0_DEFAULT
 * Number subtracted from 256 of T0 interrupts before an overflow
 *
 * Formula for calculating overflow frequency:
 * F_CPU / CLKPR(CLKPS3:CLKPS0) / TCCR0B(CS02:CS00) / (256 - TCNT0_DEFAULT)
 * 8 MHz / 8 / 64 / (256 - 130) = 124.01Hz
 * 8 MHz / 8 / 8 / (256 - 131) = 1000Hz
 */
#define TCNT0_DEFAULT 131

#define HEART_PORT PORTC
#define HEART_DDR DDRC
#define HEART PORTC2
#define DDR_HEART DDC2

/**
 * @def HEART_PERIOD
 * Number of milliseconds that the heart beat should stay in a state before
 * toggling
 */
#define HEART_PERIOD 100

#define USB_UPDATE_PERIOD 1

volatile uint32_t systemTime;

void initTimer0(void);
void initPorts(void);
int main(void);
#endif /* MAIN_H */
