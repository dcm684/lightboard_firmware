/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
#include <asf.h>

// main.c		Main program for light board firmware
//              Written by Christopher Meyer
#include "main.h"

#include "data_storage.h"
#include "display.h"
#include "drivers/UART_Serial.h"
#include "pc_interface.h"
#include "drivers/SPI.h"
#include "drivers/EEPROM_25AA640A.h"
#include "drivers/USB_Serial.h"

//#include "tests/Test_EEPROM_25AA640.h"

extern volatile bool dataInRXBuffer;
//extern uint16_t allOnScreen[];

extern bool eepromIsPopulated(void);

/**
 * Update the display
 *
 * Each call updates the display and heartbeat LED
 */
ISR(TIMER0_OVF_vect)
{
    TCNT0 = TCNT0_DEFAULT; //Reset T0 counter

    static uint16_t heartCounter;

    heartCounter++;

    if(heartCounter > HEART_PERIOD) {
        HEART_PORT ^= _BV(HEART);
        heartCounter = 0;
    }

    systemTime++;

    displayInterruptHandler();
}

/**
 * Initializes the DDR of non-comm and display pins
 */
void initPorts(void)
{
    HEART_DDR |= (1U << DDR_HEART);
}

/**
 * Initializes the timer and activates the interrupts
 *
 * Set to interrupt every 1ms
 */
void initTimer0(void)
{
    TCCR0A = 0x00; //Normal clock-only operation of timer 0
    TCNT0 = TCNT0_DEFAULT; //Set T0 counter
    OCR0A = 0x00; //Output compare register initialized, but not used
    OCR0B = 0x00; //Output compare register initialized, but not used
    TIMSK0 = 0x01; //Disable output compare interrupts; Unmask T0 overflow interrupt

    TCCR0B = 0x03; //Set Timer0 prescaler to clk_io/64 and start timer

    systemTime = 0;
}

int main(void)
{

    board_init();

    CLKPR = 0x80; //Enter CLKPR programming mode
    CLKPR = 0x00;// 0x03; // Set the system clock prescaler to 8 (CLK_IO = F_CPU / 8)

    initEEPROM(); /* Run first */
    eeprom_25aa640a_init();

    initPorts();
    initDisplay();
    initTimer0();
    spi_init();
    uart_initialize0(); /* Init before USB s/w setup to make stdio on UART */

    USB_SetupHardware();
    USB_SetupSoftware();

    stdout = uartOne;

#if 0 /* Test loop */
    sei();
    const uint16_t testPeriod = 10000; /* Sets ms between tests */
    uint16_t testCounter = testPeriod;

    for(;;) {
        if(testCounter >= testPeriod) {
            testCounter = 0;
            test_byte_write_read();
            //test_block_write_read();
        }

        testCounter++;
        _delay_ms(1);
    }

#endif /* Test loop */

#if 0
    /* If EEPROM is not populated play a basic pattern */
    if(eepromIsPopulated()) {
        
    }
#endif    

#if 0 /* Cycle through MAX_LENGTH LEDs one at a time */
    displayCycleLEDs(0, 100);
#endif

    sei(); //Enable interrupts

    for(;;) {
        USB_main_loop();

        if(dataInRXBuffer == true) {
            serial_buffer_parser();
        }
        
        /* Load the next screen from EEPROM when requested */
        if (bufferNextScreen) {
            bufferScreen();
            bufferNextScreen = false;
        }
    }
}

