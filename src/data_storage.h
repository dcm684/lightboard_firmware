#ifndef EEPROM_H
#define EEPROM_H

#include "drivers/EEPROM_25AA640A.h"

/**
 * Contains defines for the non-volatile memory
 */

/* Number of bytes needed to store a single pattern.
 * @TODO: Replace with a global value */
#define EE_PATERN_SIZE 13

/** 
 * @def EE_ADDR_ACTIVE_BANK
 * Address in EEPROM indicating the bank number actively displayed
 */
#define EE_ADDR_ACTIVE_BANK 0

/**
 * @define EE_BANK_BYTES
 * Size per bank in bytes
 */
#define EE_BANK_BYTES ((EEPROM_CAPACITY - (EE_ADDR_ACTIVE_BANK + 1)) / 2)

/**
 * @define EE_BANK_SCREENS
 * Size per bank in screens (pattern + delay)
 */
#define EE_BANK_SCREENS (EE_BANK_BYTES / (EE_PATERN_SIZE + 1))

/**
 * @def EE_ADDR_ARRAY1
 * Address to start bank 1's storage at
 */
#define EE_ADDR_ARRAY1 (EE_ADDR_ACTIVE_BANK + 1)

/**
 * @def EE_ADDR_ARRAY2
 * Address to start bank 2's storage at
 */
#define EE_ADDR_ARRAY2 (EE_ADDR_ARRAY1 + 1 + EE_BANK_BYTES)

/**
 * @def EE_OFFSET_BANK_DATA
 * Offset in bytes from bank beginning to where the screen data begins
 * 
 * Typically the max size of the screen count sets this
 */
#define EE_OFFSET_BANK_DATA 2

uint16_t calcScreenAddress(uint8_t bank, uint16_t screen);
void storeNextScreenInEEPROM(uint16_t numberOfScreens, uint8_t inPattern[EE_PATERN_SIZE], uint8_t inDelay);
uint16_t readNextScreenFromEEPROM(uint8_t *outPattern, uint8_t *outDelay);
void clearScreen(void);
bool eepromIsPopulated(void);
void initEEPROM(void);

#endif /* EEPROM_H */
